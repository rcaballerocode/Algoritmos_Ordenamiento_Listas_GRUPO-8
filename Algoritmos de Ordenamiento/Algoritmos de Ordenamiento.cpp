﻿// Ordenamientos.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"
#include <iostream>

struct tNodo;
typedef tNodo* tLista;
struct tNodo {
	int dato;
	tLista sgte;
};

int menu();

// Operaciones basicas con listas
tLista crearNodo(int dato);
void mostrarLista(tLista lista);
void destruirLista(tLista &lista);
void insertarFinal(tLista &lista, int dato);
tLista obtenerUltimoNodo(tLista lista);
int obtenerDatoNodo(tLista lista, int pos);
void cambiarDatoNodo(tLista &lista, int pos, int dato);
tLista copiarLista(tLista &lista, int inicio, int fin);
int tamanyoLista(tLista lista);

// Algoritmos de Ordenamiento

void ordenamientoIntercambio(tLista &lista);
void ordenamientoBurbuja(tLista &lista);
void ordenamientoSeleccion(tLista &lista);
void ordenamientoInsercion(tLista &lista);
void ordenamientoQuicksort(tLista &lista);
void quicksort(tLista &lista, int primero, int ultimo); // Esta funcion la usa quicksort
void ordenamientoShell(tLista &lista);
void ordenamientoMergesort(tLista &lista);
void merge(tLista &listaA, tLista &listaL, tLista &listaR); // Esta funcion la usa mergesort
void ordenamientoRadixsort(tLista &lista);

using namespace std;

int main() {

	int opc, dato;

	tLista lista = NULL;

	opc = menu();

	while (opc != 0) {
		switch (opc) {
		case 1:
			cout << "\n\t\t\t\t\tVALOR: "; cin >> dato;
			insertarFinal(lista, dato);
			mostrarLista(lista);
			break;
		case 2:
			cout << "Lista inicial" << endl;
			mostrarLista(lista);
			ordenamientoIntercambio(lista);
			cout << "Lista ordenada" << endl;
			mostrarLista(lista);
			break;
		case 3:
			cout << "Lista inicial" << endl;
			mostrarLista(lista);
			ordenamientoBurbuja(lista);
			cout << "Lista ordenada" << endl;
			mostrarLista(lista);
			break;
		case 4:
			cout << "Lista inicial" << endl;
			mostrarLista(lista);
			ordenamientoSeleccion(lista);
			cout << "Lista ordenada" << endl;
			mostrarLista(lista);
			break;
		case 5:
			cout << "Lista inicial" << endl;
			mostrarLista(lista);
			ordenamientoInsercion(lista);
			cout << "Lista ordenada" << endl;
			mostrarLista(lista);
			break;
		case 6:
			cout << "Lista inicial" << endl;
			mostrarLista(lista);
			ordenamientoQuicksort(lista);
			cout << "Lista ordenada" << endl;
			mostrarLista(lista);
			break;
		case 7:
			cout << "Lista inicial" << endl;
			mostrarLista(lista);
			ordenamientoShell(lista);
			cout << "Lista ordenada" << endl;
			mostrarLista(lista);
			break;
		case 8:
			cout << "Lista inicial" << endl;
			mostrarLista(lista);
			ordenamientoMergesort(lista);
			cout << "Lista ordenada" << endl;
			mostrarLista(lista);
			break;
		case 9:
			cout << "Lista inicial" << endl;
			mostrarLista(lista);
			ordenamientoRadixsort(lista);
			cout << "Lista ordenada" << endl;
			mostrarLista(lista);
			break;
		case 10:
			mostrarLista(lista);
			break;
		default:
			cout << "\n\t\t\tOpcion invalida! (Enter para continuar)" << endl;
			cin.get();
		}
		cout << "Enter para continuar..." << endl;
		cin.get();
		opc = menu();
	}

	cin.get();
	return 0;
}


tLista crearNodo(int dato) {
	tLista nuevo = new tNodo;
	nuevo->dato = dato;
	nuevo->sgte = NULL;

	return nuevo;
}

void mostrarLista(tLista lista) {
	if (lista == NULL) {
		cout << "Lista vacia!" << endl;
	}
	else {
		tLista p = lista;
		while (p != NULL) {
			cout << p->dato << "->";
			p = p->sgte;
		}
		cout << "NULL" << endl << endl;
	}
}

tLista copiarLista(tLista &lista, int inicio, int fin) {
	tLista listaNueva = NULL;

	while (inicio <= fin) {
		insertarFinal(listaNueva, obtenerDatoNodo(lista, inicio));
		inicio++;
	}

	return listaNueva;
}

void destruirLista(tLista &lista) {

	while (lista != NULL) {
		tLista aux = lista;
		lista = lista->sgte;
		delete aux;
	}
	
}

tLista obtenerUltimoNodo(tLista lista) {
	tLista p = lista;
	while (p->sgte != NULL) {
		p = p->sgte;
	}
	return p;
}

int obtenerDatoNodo(tLista lista, int pos) {
	tLista p = lista;
	int contador = 1;
	while (contador < pos && p->sgte != NULL) {
		p = p->sgte;
		contador++;
	}
	return p->dato;
}

void cambiarDatoNodo(tLista &lista, int pos, int dato) {
	tLista p = lista;
	int contador = 1;
	while (contador < pos && p->sgte != NULL) {
		p = p->sgte;
		contador++;
	}
	p->dato = dato;
}

int tamanyoLista(tLista lista) {
	tLista p = lista;
	if (p == NULL) {
		return 0;
	}
	else {
		int contador = 1;
		while (p->sgte != NULL) {
			p = p->sgte;
			contador++;
		}
		return contador;
	}

}

void insertarFinal(tLista &lista, int dato) {
	tLista nuevo = crearNodo(dato);

	if (lista == NULL) {
		lista = nuevo;
	}
	else {
		tLista p = lista;
		while (p->sgte != NULL) {
			p = p->sgte;
		}
		p->sgte = nuevo;
	}
}

// ALGORITMOS DE ORDENAMIENTO SOBRE LISTAS

void ordenamientoIntercambio(tLista &lista) {

	if (lista != NULL) {

		tLista ultimo = obtenerUltimoNodo(lista);
		tLista p = lista;
		tLista aux = NULL;

		while (p != ultimo) {
			aux = p->sgte;
			while (aux != NULL) {
				if (aux->dato < p->dato) {
					int temp = aux->dato;
					aux->dato = p->dato;
					p->dato = temp;
				}
				aux = aux->sgte;
			}
			p = p->sgte;
		}

	}
}

void ordenamientoBurbuja(tLista &lista) {

	if (lista != NULL) {

		tLista ultimo = obtenerUltimoNodo(lista);
		tLista primero = lista;
		tLista p = NULL;
		tLista ultimoIntercambio = NULL;

		while (ultimo != primero) {
			ultimoIntercambio = primero;
			p = lista;
			while (p != ultimo) {
				if (p->sgte->dato < p->dato) {
					int aux = p->dato;
					p->dato = p->sgte->dato;
					p->sgte->dato = aux;
					ultimoIntercambio = p;
				}
				p = p->sgte;
			}
			ultimo = ultimoIntercambio;
		}

	}
}

void ordenamientoSeleccion(tLista &lista) {

	if (lista != NULL) {

		tLista ultimo = obtenerUltimoNodo(lista);
		tLista menor = NULL;
		tLista aux = NULL;
		tLista p = lista;

		while (p != ultimo) {
			menor = p;
			aux = p->sgte;
			while (aux != NULL) {
				if (aux->dato < menor->dato) {
					menor = aux;
				}
				aux = aux->sgte;
			}
			if (p != menor) {
				int aux = p->dato;
				p->dato = menor->dato;
				menor->dato = aux;
			}
			p = p->sgte;
		}

	}
}

void ordenamientoInsercion(tLista &lista) {

	if (lista != NULL) {

		tLista lista2 = NULL;
		tLista aux = NULL;
		tLista ant = NULL;
		tLista p = lista;

		while (p != NULL) {
			tLista nuevo = crearNodo(p->dato);
			if (lista2 == NULL) {
				lista2 = nuevo;
			}else {
				if (nuevo->dato < lista2->dato) {
					nuevo->sgte = lista2;
					lista2 = nuevo;
				}else {
					aux = lista2->sgte;
					ant = lista2;
					while ((aux != NULL) && (nuevo->dato > aux->dato) ) {
						ant = aux;
						aux = aux->sgte;
					}
					ant->sgte = nuevo;
					nuevo->sgte = aux;
				}
			}
			p = p->sgte;
		}

		destruirLista(lista);
		lista = lista2;
	}
}


void ordenamientoQuicksort(tLista &lista) {

	int size = tamanyoLista(lista);
	quicksort(lista, 1, size);

}

void quicksort(tLista &lista, int primero, int ultimo) {

	int i, j, central;
	int pivote;
	central = (primero + ultimo) / 2;
	pivote = obtenerDatoNodo(lista, central);
	i = primero;
	j = ultimo;

	do {
		while (obtenerDatoNodo(lista, i)<pivote) i++; // Avanza una posicion hacia la derecha mientras el dato sea menor
		while (obtenerDatoNodo(lista, j)>pivote) j--; // Avanza una posicion hacia la izquierda mientras el dato sea mayor

		if (i <= j) { // Mientras los indices no se solapen, hay que intercambiar los valores
			int aux;
			aux = obtenerDatoNodo(lista, i);
			cambiarDatoNodo(lista, i, obtenerDatoNodo(lista, j));
			cambiarDatoNodo(lista, j, aux);
			i++; // Avanza una posicion hacia la derecha
			j--; // Avanza una posicion hacia la izquierda
		}

	} while (i <= j); // Mientras los indices no se solapen se repite el proceso

					  // Ahora ya tenemos todos los valores menores al pivote en la parte izquierda y los mayores en la parte derecha

	if (primero<j) {
		quicksort(lista, primero, j); // Llamada recursiva enviando la sublista izquierda
	}
	if (i<ultimo) {
		quicksort(lista, i, ultimo); // Llamada recursiva enviando la sublista derecha
	}

}

void ordenamientoShell(tLista &lista) {

	int size = tamanyoLista(lista);
	int intervalo, i, j, k;
	intervalo = size / 2;

	while (intervalo > 0) {
		for (i = intervalo + 1; i <= size; i++) {
			j = i - intervalo;
			while (j>0) {
				k = j + intervalo;
				if (obtenerDatoNodo(lista, j) <= obtenerDatoNodo(lista, k)) {
					j = -1;
				}
				else {
					int aux;
					aux = obtenerDatoNodo(lista, j);
					cambiarDatoNodo(lista, j, obtenerDatoNodo(lista, k));
					cambiarDatoNodo(lista, k, aux);
					j = j - intervalo;
				}
			}
		}
		intervalo = intervalo / 2;
	}

}

void ordenamientoMergesort(tLista &lista) {
	if (tamanyoLista(lista)>1) {
		int mitad = tamanyoLista(lista) / 2;

		tLista listaL = copiarLista(lista, 1, mitad);
		tLista listaR = copiarLista(lista, mitad+1, tamanyoLista(lista));

		ordenamientoMergesort(listaL);
		ordenamientoMergesort(listaR);

		merge(lista, listaL, listaR);
	}

}

void merge(tLista &lista, tLista &listaL, tLista &listaR) {
	int totalElem = tamanyoLista(listaL) + tamanyoLista(listaR);

	int i, li, ri;
	i = li = ri = 1;
	while (i <= totalElem) {
		if ((li <= tamanyoLista(listaL)) && (ri <= tamanyoLista(listaR))) {
			if (obtenerDatoNodo(listaL,li) < obtenerDatoNodo(listaR, ri)) {
				cambiarDatoNodo(lista, i, obtenerDatoNodo(listaL, li));
				i++;
				li++;
			}
			else {
				cambiarDatoNodo(lista, i, obtenerDatoNodo(listaR, ri));
				i++;
				ri++;
			}
		}
		else {
			if (li > tamanyoLista(listaL)) {
				while (ri <= tamanyoLista(listaR)) {
					cambiarDatoNodo(lista, i, obtenerDatoNodo(listaR, ri));
					i++;
					ri++;
				}
			}
			if (ri > tamanyoLista(listaR)) {
				while (li <= tamanyoLista(listaL)) {
					cambiarDatoNodo(lista, i, obtenerDatoNodo(listaL, li));
					li++;
					i++;
				}
			}
		}
	}
}

int cantidadDigitos(int num)
{
	int x = 0;
	while (num != 0)
	{
		num = num / 10;
		x = x + 1;
	}
	return x;
}

int mayor(tLista &lista) { // Recorre la lista para buscar el numero con mayor digitos

	tLista aux = lista;
	int mayor = -1;

	while (aux != NULL) {

		if (cantidadDigitos(aux->dato) > mayor) {
			mayor = cantidadDigitos(aux->dato);
		}

		aux = aux->sgte;
	}

	return mayor;

}

void ordenamientoRadixsort(tLista &lista) {

	tLista listaAuxiliar = NULL;

	tLista aux = lista;
	int exp, k, num, digito;

	int maximo = mayor(lista);


	for (int k = 1; k <= maximo; k++) {

		tLista listas[10] = { NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL };

		while (aux != NULL) {

			num = aux->dato;
			exp = pow(10, k - 1); /* elevar 10 a la (k-1)ésima potencia */
			digito = (num / exp) % 10;

			insertarFinal(listas[digito], aux->dato);
			aux = aux->sgte;

		}

		for (int i = 0; i < 10; i++) {

			if (listas[i] != NULL) {
				tLista q = listas[i];
				while (q != NULL) {
					insertarFinal(listaAuxiliar, q->dato);
					q = q->sgte;
				}
			}
		}
		aux = listaAuxiliar;
		listaAuxiliar = NULL;
	}

	lista = aux;

}



int menu() {

	int opc;

	system("cls");
	cout << endl;
	cout << "\t\t\tÉÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ»" << endl;
	cout << "\t\t\tº                MENU              º" << endl;
	cout << "\t\t\tÌÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ¹" << endl;
	cout << "\t\t\tº  1. Adicionar elemento (al final)º" << endl;
	cout << "\t\t\tº  2. Ordenar por intercambio      º" << endl;
	cout << "\t\t\tº  3. Ordenar por burbuja          º" << endl;
	cout << "\t\t\tº  4. Ordenar por seleccion        º" << endl;
	cout << "\t\t\tº  5. Ordenar por insercion        º" << endl;
	cout << "\t\t\tº  6. Ordenar por quicksort        º" << endl;
	cout << "\t\t\tº  7. Ordenar por shellsort        º" << endl;
	cout << "\t\t\tº  8. Ordenar por mergesort        º" << endl;
	cout << "\t\t\tº  9. Ordenar por radixsort        º" << endl;
	cout << "\t\t\tº  10. Mostrar lista               º" << endl;
	cout << "\t\t\tÈÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ¼" << endl;

	cout << "\t\t\t\tIngrese una opcion: ";
	cin >> opc;

	cin.get();

	return  opc;
}